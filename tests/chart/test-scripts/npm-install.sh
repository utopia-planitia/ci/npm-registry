#!/bin/bash
set -euxo pipefail

mkdir /workspace
cd /workspace

npm config set registry http://npm-registry.npm-registry.svc --global
npm install express
